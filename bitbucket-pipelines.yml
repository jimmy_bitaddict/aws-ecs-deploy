image:
  name: python:3.7

setup: &setup
  step:
    name: Setup testing resources
    script:
      - STACK_NAME_CLUSTER="bbci-test-ecr-cluster-${BITBUCKET_BUILD_NUMBER}"
      - pipe: atlassian/aws-cloudformation-deploy:0.6.1
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: "us-east-1"
          STACK_NAME: ${STACK_NAME_CLUSTER}
          TEMPLATE: "./test/CloudformationStackTemplate_ecs_cluster.yml"
          CAPABILITIES: ['CAPABILITY_IAM']
          WAIT: 'true'
          STACK_PARAMETERS: >
            [{
              "ParameterKey": "ClusterNameCustom",
              "ParameterValue": ${STACK_NAME_CLUSTER}
            }]
          TAGS: >
              [{
                "Key": "owner",
                "Value": "bbci-test-ecr-infrastructure"
              }]

      - STACK_NAME_SERVICE="bbci-test-ecr-service-${BITBUCKET_BUILD_NUMBER}"
      - pipe: atlassian/aws-cloudformation-deploy:0.6.1
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: "us-east-1"
          STACK_NAME: ${STACK_NAME_SERVICE}
          TEMPLATE: "./test/CloudformationStackTemplate_ecs_service.yml"
          CAPABILITIES: ['CAPABILITY_IAM']
          WAIT: 'true'
          STACK_PARAMETERS: >
            [
              {
                "ParameterKey": "StackName",
                "ParameterValue": ${STACK_NAME_CLUSTER}
              },
              {
                "ParameterKey": "ServiceName",
                "ParameterValue": ${STACK_NAME_SERVICE}
              }
            ]
          TAGS: >
              [{
                "Key": "owner",
                "Value": "bbci-test-ecr-infrastructure"
              }]


test: &test
  parallel:
    - step:
        name: Test
        caches:
          - pip
        script:
        - pip install -r test/requirements.txt
        
        - pytest test/test.py --verbose --capture=no --junitxml=test-reports/report.xml
        - flake8 --ignore E501,E125
        after-script:
          - STACK_NAME_SERVICE="bbci-test-ecr-service-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" cloudformation delete-stack --stack-name ${STACK_NAME_SERVICE}
          - aws --region "us-east-1" cloudformation wait stack-delete-complete --stack-name ${STACK_NAME_SERVICE}
          - STACK_NAME_CLUSTER="bbci-test-ecr-cluster-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" cloudformation delete-stack --stack-name ${STACK_NAME_CLUSTER}
        services:
        - docker
    - step:
        name: Lint the Dockerfile
        image: hadolint/hadolint:latest-debian
        script:
          - hadolint Dockerfile


releas-dev: &release-dev
  step:
    name: Release development version
    script:
      - set -ex
      - pip install semversioner
      - VERSION=$(semversioner current-version)
      - IMAGE=bitbucketpipelines/$BITBUCKET_REPO_SLUG
      - echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
      - docker build -t ${IMAGE} .
      - docker tag ${IMAGE} ${IMAGE}:${VERSION}-dev
      - docker push ${IMAGE}:${VERSION}-dev
    services:
      - docker


push: &push
  step:
    name: Push and Tag
    script:
      - pipe: docker://bitbucketpipelines/bitbucket-pipe-release:1.0.0
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/${BITBUCKET_REPO_SLUG}


pipelines:
  default:
  - <<: *setup
  - <<: *test
  - <<: *release-dev
  branches:
    master:
    - <<: *setup
    - <<: *test
    - <<: *push
